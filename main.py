from base64 import b64encode
from tkinter import *
import tkinter.filedialog   #модуль filedialog для диалогов открытия/закрытия файла
import pyAesCrypt
import io

def Quit(ev):
    global root
    root.destroy()
    
def LoadFile(ev):
    ftypes = [('Все файлы', '*'), ('txt файлы', '*.txt'), ('Файлы Python', '*.py'), ('Файлы html', '*.html')] # Фильтр файлов
    fn = tkinter.filedialog.Open(root, filetypes = ftypes).show()
    
    if fn == '':
        return  
    textbox.delete('1.0', 'end')    # Очищаем окно редактирования
    textbox.insert('1.0', open(fn).read())   # Вставляем текст в окно редактирования
   
     
    global cur_path
    cur_path = fn # Храним путь к открытому файлу
   
def SaveFile(ev):
    fn = tkinter.filedialog.SaveAs(root, filetypes = [('Все файлы', '*'), ('txt файлы', '*.txt'), ('Файлы Python', '*.py'), ('Файлы html', '*.html')]).show()
    if fn == '':
        return
    open(fn, 'wt').write(textbox.get('1.0', 'end'))

def Encode(ev):
    s = textbox.get('1.0', END)
    s = bytes(s, 'utf-8')
    encoded = b64encode(s)
    textbox.delete('1.0', 'end') 
    textbox.insert('1.0', encoded)

def AesEncoding(en):
    s = textbox.get('1.0', END)
    s = bytes(s, 'utf-8')
    fIn = io.BytesIO(s)
    fCiph = io.BytesIO()
    key = key_mes.get()
    bufferSize = 512*1024
    pyAesCrypt.encryptStream(fIn, fCiph, key, bufferSize)
    textbox.delete('1.0', 'end') 
    textbox.insert('1.0', str(fCiph.getvalue()))

    
root = Tk()   
root.title(u'Текстовый редактор (.py, .txt, .html)')
key_mes = StringVar()

panelFrame = Frame(root, height = 60, bg = 'gray')
textFrame = Frame(root, height = 340, width = 600)

panelFrame.pack(side = 'top', fill = 'x')   #упакуем с привязкой к верху
textFrame.pack(side = 'bottom', fill = 'both', expand = 1)  

textbox = Text(textFrame, font='Arial 14', wrap='word')  #перенос по словам метод wrap
scrollbar = Scrollbar(textFrame)

scrollbar['command'] = textbox.yview
textbox['yscrollcommand'] = scrollbar.set

textbox.pack(side = 'left', fill = 'both', expand = 1)  #текстбокс слева
scrollbar.pack(side = 'right', fill = 'y')    #расположим скролбар (лифт) справа

loadBtn = Button(panelFrame, text = 'Загрузить')
saveBtn = Button(panelFrame, text = 'Сохранить')
decodeBtn = Button(panelFrame, text = 'Шифровать')
key_entry = Entry(textvariable=key_mes)
aesdecoce = Button(panelFrame, text = 'Aes шифр')

quitBtn = Button(panelFrame, text = 'Выход', bg='#A9A9A9',fg='#FF0000')

loadBtn.bind("<Button-1>", LoadFile)
saveBtn.bind("<Button-1>", SaveFile)
decodeBtn.bind("<Button-1>", Encode)
aesdecoce.bind("<Button-1>", AesEncoding)
quitBtn.bind("<Button-1>", Quit)

loadBtn.place(x = 10, y = 10, width = 130, height = 40)
saveBtn.place(x = 150, y = 10, width = 130, height = 40)
decodeBtn.place(x = 290, y = 10, width = 130, height = 40)
aesdecoce.place(x = 430, y = 10, width = 130, height = 40)
key_entry.place(x = 570, y = 10, width = 130, height = 40)
quitBtn.place(x = 790, y = 10, width = 100, height = 40)
root.mainloop()

